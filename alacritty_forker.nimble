# Package
version       = "0.1.0"
author        = "Rasmus Moorats"
description   = "forks Alacritty from existing windows"
license       = "GPL-3.0-or-later"
srcDir        = "src"
bin           = @["alacritty_forker"]

# Dependencies
requires "nim >= 1.6.6"
requires "https://git.dog/xx/nim-swayipc.git#head"

let outdir = "bin/"
let output = outdir & bin[0]

task release, "build binary for release":
  exec "mkdir -p " & outdir
  exec "nim c -d:release --passC:-flto --passL:-flto --passL:-s --opt:speed --mm:orc --passC:-ffast-math -o:" & output & " src/" & bin[0] & ".nim"
  echo "built " & output

task clean, "clean workspace":
  exec "rm " & output
  exec "rm -r " & outdir
