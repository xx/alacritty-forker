import os
import posix
import system
import net
import swayipc2/[connection, commands]

proc main() =
  let sockpath = "/run/user/" & $geteuid() & "/Alacritty-*.sock"
  for sock in walkPattern(sockpath): # we only expect this loop to be executed 0 or 1 times
    let s = newSocket(Domain.AF_UNIX, SockType.SOCK_STREAM, Protocol.IPPROTO_IP)
    s.connectUnix(sock)
    s.send("{\"CreateWindow\":{\"terminal_options\":{\"working_directory\":\"" & getHomeDir() & "\",\"hold\":false,\"command\":[]},\"window_identity\":{\"title\":null,\"class\":null}}}")
    close s
    system.quit(0)

  let sway = connect()

  let ret = sway.run_command("exec alacritty")[0]
  sway.close
  system.quit(if ret.success: 0 else: 2)

when isMainModule:
  main()
